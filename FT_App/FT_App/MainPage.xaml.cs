﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace FT_App
{
	
	public partial class MainPage : ContentPage
    {
		//public ZXingBarcodeImageView BCGO;
		ZXingBarcodeImageView barcode;
        public MainPage()
        {
            InitializeComponent();
			BindingContext = this;
			MyStringProperty= "https://www.t-mobile.com/cell-phone/apple-iphone-13?sku=194252705452";

		}
        private string myStringProperty;
        public string MyStringProperty
        {
            get { return myStringProperty; }
            set
            {
                myStringProperty = value;
                OnPropertyChanged(nameof(MyStringProperty)); // Notify that there was a change on this property
            }
        }
		public void Pink_Clicked(object sender, EventArgs args)
		{
			Selected_Price.Text = "   $399.99";
			Selected_Color.Text = "Pink Selected:";
			Phone_Variant.Text = "Pink Selected:";
			MyStringProperty = "https://www.t-mobile.com/cell-phone/apple-iphone-13?sku=194252705452";
		}
		public void Silver_Clicked(object sender, EventArgs args)
		{
			Selected_Price.Text = "    $998.99";
			Selected_Color.Text = "Starlight Selected:";
			Phone_Variant.Text = "Starlight Selected:";
			MyStringProperty = "https://www.t-mobile.com/cell-phone/apple-iphone-13?sku=194252705421";
		}
		public void CadetBlue_Clicked(object sender, EventArgs args)
		{
			Selected_Price.Text = "  $1100.99";
			Selected_Color.Text = "Blue Selected:";
			Phone_Variant.Text = "Blue Selected:";
			MyStringProperty = "https://www.t-mobile.com/cell-phone/apple-iphone-13?sku=194252705513";
		}
		public void Red_Clicked(object sender, EventArgs args)
		{
			Selected_Price.Text = "   $999.99";
			Selected_Color.Text = "Red Selected:";
			Phone_Variant.Text = "Red Selected:";
			MyStringProperty = "https://www.t-mobile.com/cell-phone/apple-iphone-13?sku=194252705483";
		}
		public void Black_Clicked(object sender, EventArgs args)
		{
			Selected_Price.Text = "   $1129.99";
			Selected_Color.Text = "Midnight Selected:";
			Phone_Variant.Text = "Midnight Selected:";
			MyStringProperty = "https://www.t-mobile.com/cell-phone/apple-iphone-13?sku=194252705391";
		}
		public void GetBarcode(string BarcodeText)
        {
			Device.BeginInvokeOnMainThread(() =>
			{
				barcode = new ZXingBarcodeImageView
				{
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
					AutomationId = "BarCodeId",
				};
				barcode.BarcodeFormat = ZXing.BarcodeFormat.QR_CODE;
				barcode.BarcodeOptions.Width = 300;
				barcode.BarcodeOptions.Height = 300;
				barcode.BarcodeOptions.Margin = 10;
				barcode.BarcodeValue = BarcodeText;

				BarCodeId = barcode;
			});
		}
		public void BarcodePage()
		{
			var stackLayout = new StackLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			barcode = new ZXingBarcodeImageView
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				AutomationId = "zxingBarcodeImageView",
			};
			barcode.BarcodeFormat = ZXing.BarcodeFormat.QR_CODE;
			barcode.BarcodeOptions.Width = 300;
			barcode.BarcodeOptions.Height = 300;
			barcode.BarcodeOptions.Margin = 10;
			barcode.BarcodeValue = "ZXing.Net.Mobile";

			var text = new Label
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Text = "ZXing Sample"
			};
			//text.TextChanged += Text_TextChanged;

			stackLayout.Children.Add(barcode);
			stackLayout.Children.Add(text);

			Content = stackLayout;
		}
		void Text_TextChanged(object sender, TextChangedEventArgs e)
			=> barcode.BarcodeValue = e.NewTextValue;
	}
}
